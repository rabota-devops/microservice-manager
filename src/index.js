import {run} from '@cycle/rxjs-run';
import {makeMqttDriver} from './driver/mqtt';
import {makeDOMDriver} from '@cycle/dom';
import {App} from './app';

const main = App;

const drivers = {
    DOM: makeDOMDriver('#root'),
    MQTT: makeMqttDriver({
        host: 'ws://localhost:15675/ws',
        channel: '#',
        pubChannel: 'rabot/chat/user'
    })
};

run(main, drivers);
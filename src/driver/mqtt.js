import {connect} from 'async-mqtt';
import {fromEvent} from 'rxjs';

export function makeMqttDriver({host, channel, pubChannel}) {
    const client = connect(host);
    client.subscribe(channel);

    function mqttDriver (outgoing$) {
        outgoing$.addListener({
            next: message => client.publish(pubChannel, message)
        });

        return fromEvent(client, 'message')
    }

    return mqttDriver;
}
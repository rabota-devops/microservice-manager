import {interval, merge} from 'rxjs';
import {startWith, map, scan} from 'rxjs/operators';

const intent = ({MQTT}) => ({
    timer$: interval(1000).pipe(
        map(number => ({
            type: 'time',
            message: `+ ${number}`,
            channel: 'T',
        }))
    ),
    message$: MQTT.pipe(
        map(([channel, message]) => ({
            channel,
            message: message.toString(),
            type: 'message'
        }))
    )
});

const model = ({timer$, message$}) => {
    const timerAndMessages$ = merge(timer$, message$);

    return timerAndMessages$.pipe(
        scan((acc, curr) => [curr, ...acc], [])
    );
};

const view = (state$) => state$.pipe(
    startWith([]),
    map(items => <ul>
        {items.map(({channel, message, timestamp}) =>
            <li>{channel}: {message}</li>
        )}
    </ul>)
);

export const App = (sources) => ({
    DOM: view(model(intent(sources)))
});